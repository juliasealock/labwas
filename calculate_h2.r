## H2
print("loading packages")
library(rms)
library(data.table)

date <- Sys.Date()
pheno.temp <- paste0(out.dir,"pheno_temp/")
dir.create(pheno.temp)

results.dir <- paste0(out.dir,"heritability_results/")
dir.create(results.dir)

print("loading data")
pheno <- read.delim(pheno_file, header=T)
cov <- read.table(cov_file, header=T, sep="\t")
icd <- fread(icd_file, header=T)
dob <- fread(dob_file, header=T)

print("create phenotype files")
for(i in 2:ncol(pheno)){
   lab <- pheno[,c(1,i)]
   lab$FID <- "0"
   lab <- lab[c(3,1,2)]
   colnames(lab)[2] <- "IID"
   lab.name <- colnames(pheno)[i]
   lab <- lab[complete.cases(lab),]
   write.table(lab, file=paste0(pheno.temp,i,"_",lab.name,".txt"), col.names=T, row.names=F, sep="\t", quote=F)
}

print("create covariate files")
if(age_adjustment=="direct"){
    most_recent_date <- aggregate(ICD_DATE ~ GRID, FUN=median, data=icd)
    dob2 <- dob[,c("GRID","DOB")]
    recent_dob <- merge(most_recent_date, dob2, by="GRID")
    recent_dob$age <- as.numeric(as.Date(recent_dob$ICD_DATE, "%Y-%m-%d") - as.Date(recent_dob$DOB, "%Y-%m-%d"))/365.25
    age.last <- setDT(recent_dob)[,c("GRID","age")]

    ## nk=3, then agedesign have 3 -2 = 1 columns
    agedesign <- data.frame(rcspline.eval(age.last$age,nk=4))
    agespline <- cbind(age.last$GRID, agedesign)
    colnames(agespline) <- c("IID","cubic_splines_median_ehr_age1","cubic_splines_median_ehr_age2")

    cov$FID <- "0"
    colnames(cov)[1] <- "IID"
    pcs <- setDT(cov)[,c("FID","IID","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")]

    ## write covariates for direct int
    cov.direct <- merge(pcs, agespline, by="IID")
    cov.direct2 <- cov.direct[,c("FID","IID","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","cubic_splines_median_ehr_age1","cubic_splines_median_ehr_age2")]
    write.table(cov.direct2, paste0(out.dir,"direct_int_median_ehr_age_4splines_",date,".txt"), col.names=T, row.names=F, sep="\t", quote=F)
    
}

if(age_adjustment=="indirect"){
    cov$FID <- "0"
    colnames(cov)[1] <- "IID"
    pcs <- setDT(cov)[,c("FID","IID","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")]
    write.table(pcs, paste0(out.dir2, "PCs.covariates.filt1_",date,".txt"), col.names=T, row.names=F, sep="\t")

}

cov$FID <- "0"
sex <- setDT(cov)[,c("FID","GRID","GENDER")]
colnames(sex)[2] <- "IID"
write.table(sex, paste0(out.dir,"sex.covariate_",date,".txt"), col.names=T, row.names=F, sep="\t", quote=F)


if(age_adjustment=="direct"){
    qcovar_file <- paste0(out.dir,"direct_int_median_ehr_age_4splines_",date,".txt")
}
if(age_adjustment=="indirect"){
    qcovar_file <- paste0(out.dir,"sex.covariate_",date,".txt")
}
n_jobs = ncol(pheno)

print("run slurm job")
## REML Slurm for h2

slurm=paste0("#!/bin/bash
#SBATCH -n1
#SBATCH --mem=40GB
#SBATCH --time=04:00:00
#SBATCH --cpus-per-task 3
#SBATCH --array=2-",n_jobs,"
#SBATCH --output=",paste0(out.dir,"h2_reml_output.txt"),"
#SBATCH --mail-user=",email,"
#SBATCH --mail-type=ALL
#SBATCH --job-name=lab_h2
#SBATCH --account=davis_lab

phenoname=$(awk 'FNR == 1 {print $3}' ",out.dir,"/pheno_temp/${SLURM_ARRAY_TASK_ID}_*.txt)

/data/davis_lab/sealockj/projects/scripts/lab_gwas/gcta_1.92.4beta/gcta64 \\
--pheno ",out.dir,"/pheno_temp/${SLURM_ARRAY_TASK_ID}_${phenoname}.txt \\
--grm /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/mega_grm/20200330_mega_grm_relatedness_cutoff_0.05 \\
--qcovar ",qcovar_file," \\
--covar ",paste0(out.dir,"sex.covariate_",date,".txt")," \\
--reml \\
--out ",results.dir,"/${phenoname} \\
--thread-num 10 ")

slurm.path=file.path(out.dir,paste0("lab_h2.slurm"))
write(slurm, file=slurm.path)

cmd=paste0("sbatch ", slurm.path)
system(cmd)
