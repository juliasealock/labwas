library(ggplot2)

print("extracting heritability")
setwd(paste0(path))
file.names <- dir(path, pattern =".hsq")
heritability <- data.frame(matrix(ncol=2, nrow=length(file.names)))
for(i in 1:length(file.names)){
    dat <- read.table(file.names[i], header=T, sep="\t", comment.char="", fill=T)
    heritability[i,1] <- paste0(file.names[i])
    heritability[i,2] <- dat[4,2]
}

heritability$name <- gsub(".hsq", "", heritability$X1)
file.names <- dir(path, pattern =".log")
names <- data.frame(matrix(ncol=1, nrow=length(file.names)))
for(i in 1:length(file.names)){
    names[i,1] <- paste0(file.names[i])
}

colnames(heritability)[2]<- "h2"

print("find labs where h2 calculation failed")
colnames(names)[1] <- "X1"
names$name <- gsub(".log", "", names$X1)
missing <- names[!(names$name %in% heritability$name),]
missing$h2 <- "NA"

print("generating plots")
## histogram
pdf(paste0(out_dir,"/lab_h2_histogram.pdf"))
p <- ggplot(heritability, aes(x=h2)) + geom_histogram(binwidth=0.01)
print(p)
dev.off()

## find heritable labs 
heritable.labs <- subset(heritability, h2>1e-6 & h2<0.99999)

pdf(paste0(out_dir,"/lab_h2_heritable_subset_histogram.pdf"))
p1 <- ggplot(heritable.labs, aes(x=h2)) + geom_histogram(binwidth=0.01)
print(p1)
dev.off()

print("finding sample sizes")
## find h2 sample size
pheno <- read.delim(pheno_file, header=T)
colnames(pheno)[1] <- "grid"

## use this to find the N of the h2 analyses
h2_grm <- read.table("/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/mega_grm/20200330_mega_grm_relatedness_cutoff_0.05.grm.id", header=F, sep="")[c(2)]
colnames(h2_grm) <- "grid"
pheno.h2.grm <- merge(h2_grm, pheno, by="grid")

names <- names(pheno.h2.grm)
names_df <- as.data.frame(names)
n <- lapply(pheno.h2.grm, function(x) length(na.omit(x)))
names_n <- names(n)
names_n_df <- as.data.frame(names_n)
n_list <- list(n)
n_df <- data.frame(matrix(unlist(n_list)))
n <- data.frame(names_n_df, n_df)
colnames(n) <- c("name", "N")

heritability2 <- rbind(heritability, missing)
herit_n <- merge(heritability2, n, by="name")
herit_n$X1 <- NULL

write.table(herit_n, paste0(out_dir,name,"_lab_h2_and_sample_sizes.txt"), col.names=T, row.names=F, sep="\t", quote=F)

print("making labwas files")
## filter for h2 labs ##
labwas.labs <- pheno[colnames(pheno) %in% heritable.labs$name]
id.direct <- pheno[c(1)]
labwas.labs.final <- cbind(id.direct, labwas.labs)

saveRDS(labwas.labs.final, paste0(out_dir,name,"_heritable_labs_for_labwas.RDS"))
saveRDS(pheno, paste0(out_dir,name,"_all_labs_for_labwas.RDS"))
