LabWAS readme updated 06/11/2020

- added sample size restriction command 

Commands: 

prs <- “path/to/pgs” #ID in 1st column, PGS in 2nd
pheno.name <- “pheno_name”
working.dir <- “/path/to/out/dir/”
labs <- “/path/to/rds/versions/of/lab/files/“ # see below for paths
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv" #newest category files for all sd-wide labs
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")
covar <- “/path/to/covariates/file/“
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

Output:
.csv file with statistics
Manhattan graph (.pdf)

SD-wide updated labs files for LabWAS 06/11/2020

Heritable Labs:
Inverse Normal & Age Adjusted: /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS

Inverse Normal: /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/direct_int_h2/20200413_InverseNormal_Medians_direct_int_heritable_labs.RDS

All Labs:
Inverse Normal & Age Adjusted: /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_all_labs.RDS

Inverse Normal: /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/direct_int_h2/20200413_InverseNormal_Medians_direct_int_all_labs.RDS


