## create labwas files

Creating labwas files is a 3 step process:
1. calculate heritability using REML in GCTA 
    this step uses qualitylab output and runs an array job on slurm to calculate heritability
    a "pheno_temp/" directory is created with a txt file for each lab value. this temp directory can be deleted after creating labwas files 
    this step also creates a "heritability_results/" directory where REML output is stored. can also be deleted after creating labwas files
2. troubleshoot the heritability calculation 
    this step finds labs that are either sex specific, have lower power, or timed out and re-submits 
3. find heritable labs, output summary statistics, create labwas files 
    this step creates output files 

Output files:
1. histograms of heritability estimates across all labs and subset to heritable labs 
2. text file with heritability estimates and sample sizes 
3. RDS files for LabWAS - one file with only heritable labs, one file with all labs

Version of LabWAS files:
1. Direct inverse normal transformed (direct INT)
    - uses non-age adjusted median labs
2. Indirect inverse normal transformed (indirect INT)
    - uses age adjusted median labs 

Files you need:
Step 1:
    1. phenotype file with median lab values. this is output from qualitylab 
    2. covariate file with PCs and sex 
    3. (if direct INT) ICD file 
    4. (if direct INT) DOB file 

Step 2:
    1. same phenotype file from step 1
    2. covariate files outputted from step 1

Step 3:
    1. same phenotype file 

##############
## examples ##
##############


### direct INT labs 
#### use non-age adjusted labs file
#### set age_adjustment to "direct"

#### calculate h2
out.dir <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/direct_int/"
pheno_file <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20210806_sdwide_pull/selected_lab_summaries_sd20210806/20220115_Inverse_Normal_Medians_all_labs.txt"
cov_file <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt"
email <- "julia.m.sealock@vanderbilt.edu"
age_adjustment="direct"
icd_file <- "/data/davis_lab/shared/phenotype_data/biovu/delivered_data/sd_wide_pull/20210806_pull/icd_codes.csv.gz" #if direct age_adj 
dob_file <- "/data/davis_lab/shared/phenotype_data/biovu/delivered_data/sd_wide_pull/20210806_pull/demographics.csv.gz" #if direct age_adj
source("/data/davis_lab/sealockj/projects/scripts/lab_gwas/calculate_h2_of_labs.r")


# troubleshoot h2: find jobs that failed and re-run if sex-specific or if timed out
out.dir <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/direct_int/"
pheno_file <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20210806_sdwide_pull/selected_lab_summaries_sd20210806/20220115_Inverse_Normal_Medians_all_labs.txt"
cov_file <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt"
qcovar_file <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/direct_int/direct_int_median_ehr_age_4splines_2022-01-19.txt"
sex_cov_file <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/direct_int/sex.covariate_2022-01-19.txt"
email <- "julia.m.sealock@vanderbilt.edu"
source("/data/davis_lab/sealockj/projects/scripts/lab_gwas/troubleshoot_h2.r")


#### find heritable labs, plot, save labwas files
path <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/direct_int/heritability_results/"
pheno_file <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20210806_sdwide_pull/selected_lab_summaries_sd20210806/20220115_Inverse_Normal_Medians_all_labs.txt"
name <- "20220115_Inverse_Normal_Medians"
out_dir <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/direct_int/" 
source("/data/davis_lab/sealockj/projects/scripts/lab_gwas/find_heritable_labs_and_create_labwas_files.r")


### indirect INT labs 
#### use age adjusted labs file
#### set age_adjustment to "indirect"

#### calculate h2
out.dir <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/indirect_int/"
pheno_file <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20210806_sdwide_pull/selected_lab_summaries_sd20210806/20220115_Inverse_Normal_Medians_ageAdjusted_all_labs.txt"
cov_file <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt"
email <- "julia.m.sealock@vanderbilt.edu"
age_adjustment="indirect"
source("/data/davis_lab/sealockj/projects/scripts/lab_gwas/calculate_h2_of_labs.r")

# troubleshoot h2: find jobs that failed and re-run if sex-specific or if timed out
out.dir <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/indirect_int/"
pheno_file <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20210806_sdwide_pull/selected_lab_summaries_sd20210806/20220115_Inverse_Normal_Medians_all_labs.txt"
cov_file <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt"
qcovar_file <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/indirect_int/PCs.covariates.filt1_2022-01-20.txt"
sex_cov_file <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/indirect_int/sex.covariate_2022-01-20.txt"
email <- "julia.m.sealock@vanderbilt.edu"
source("/data/davis_lab/sealockj/projects/scripts/lab_gwas/troubleshoot_h2.r")


#### find heritable labs, plot, save labwas files
path <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/indirect_int/heritability_results/"
pheno_file <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20210806_sdwide_pull/selected_lab_summaries_sd20210806/20220115_Inverse_Normal_Medians_ageAdjusted_all_labs.txt"
name <- "20220115_Inverse_Normal_Medians_ageAdjusted"
out_dir <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide_20220115_pull/indirect_int/" 
source("/data/davis_lab/sealockj/projects/scripts/lab_gwas/find_heritable_labs_and_create_labwas_files.r")
