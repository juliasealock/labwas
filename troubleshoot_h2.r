## script will find labs where h2 wasn't calculated 
## discard labs from h2 analyses with sample size <100
## re-run h2 for sex-specific labs 



library(data.table)

print("find labs where h2 calculation failed")
path <- paste0(out.dir, "/heritability_results/")
setwd(paste0(path))
file.names <- dir(path, pattern =".hsq")
heritability <- data.frame(matrix(ncol=2, nrow=length(file.names)))
for(i in 1:length(file.names)){
    dat <- read.table(file.names[i], header=T, sep="\t", comment.char="", fill=T)
    heritability[i,1] <- paste0(file.names[i])
    heritability[i,2] <- dat[4,2]
}

heritability$name <- gsub(".hsq", "", heritability$X1)
file.names <- dir(path, pattern =".log")
names <- data.frame(matrix(ncol=1, nrow=length(file.names)))
for(i in 1:length(file.names)){
    names[i,1] <- paste0(file.names[i])
}

colnames(names)[1] <- "X1"
names$name <- gsub(".log", "", names$X1)
missing <- names[!(names$name %in% heritability$name),]
print(paste0("number of labs with failed h2 calculation: ", nrow(missing)))

print("finding sample sizes")
## find h2 sample size
pheno <- read.delim(pheno_file, header=T)
colnames(pheno)[1] <- "grid"

h2_grm <- read.table("/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/mega_grm/20200330_mega_grm_relatedness_cutoff_0.05.grm.id", header=F, sep="")[c(2)]
colnames(h2_grm) <- "grid"
pheno.h2.grm <- merge(h2_grm, pheno, by="grid")

names <- names(pheno.h2.grm)
names_df <- as.data.frame(names)
n <- lapply(pheno.h2.grm, function(x) length(na.omit(x)))
names_n <- names(n)
names_n_df <- as.data.frame(names_n)
n_list <- list(n)
n_df <- data.frame(matrix(unlist(n_list)))
n <- data.frame(names_n_df, n_df)
colnames(n) <- c("name", "N")


print("discard labs with N<100")
missing_n <- merge(missing, n, by="name")
missing_n_100 <- subset(missing_n, N>=100)

print(paste0("number of labs with failed h2 calculation and N>=100: ", nrow(missing_n_100)))

print("find sex-specific labs and re-run h2 calculation")
cov <- fread(cov_file, header=T, sep="\t")
sex <- cov[,c("GRID","GENDER")]

missing.labs <- pheno[colnames(pheno) %in% missing_n_100$name]
id <- pheno[c(1)]
missing.labs2 <- cbind(id, missing.labs)
missing.labs.sex <- merge(sex, missing.labs2, by.x="GRID", by.y="grid")
missing.labs.sex.df <- as.data.frame(missing.labs.sex)

sex.n <- data.frame(matrix(ncol=3, nrow=ncol(missing.labs.sex.df)))
for(i in 3:ncol(missing.labs.sex.df)){
    lab.name <- colnames(missing.labs.sex.df)[[i]]
    lab <- missing.labs.sex.df[,c("GRID","GENDER",lab.name)]
    lab.complete <- lab[complete.cases(lab[,3]),]
    sex.n[i,1] <- paste0(lab.name)
    sex.n[i,2] <- paste0(nrow(subset(lab.complete, GENDER=="M")))
    sex.n[i,3] <- paste0(nrow(subset(lab.complete, GENDER=="F")))
}

colnames(sex.n) <- c("lab","n_male","n_female")
sex.n.complete <- sex.n[complete.cases(sex.n),]
sex.spec <- subset(sex.n.complete, n_male==0 | n_female==0)

print(paste0("number of sex-specific labs:", nrow(sex.spec)))

if(nrow(sex.spec>0)){
    print("re-run h2 for sex-specific labs")
    sex.spec.job.numbers <- match(sex.spec$lab, names(pheno))
    sex.spec.job.numbers2 <- paste0(sex.spec.job.numbers, collapse=",")

    results.dir <- path

    slurm=paste0("#!/bin/bash
#SBATCH -n1
#SBATCH --mem=40GB
#SBATCH --time=12:00:00
#SBATCH --cpus-per-task 3
#SBATCH --array=",sex.spec.job.numbers2,"
#SBATCH --output=",paste0(out.dir,"sex_specific_h2_reml_output.txt"),"
#SBATCH --mail-user=",email,"
#SBATCH --mail-type=ALL
#SBATCH --job-name=sex_spec_lab_h2
#SBATCH --account=davis_lab

phenoname=$(awk 'FNR == 1 {print $3}' ",out.dir,"/pheno_temp/${SLURM_ARRAY_TASK_ID}_*.txt)

/data/davis_lab/sealockj/projects/scripts/lab_gwas/gcta_1.92.4beta/gcta64 \\
--pheno ",out.dir,"/pheno_temp/${SLURM_ARRAY_TASK_ID}_${phenoname}.txt \\
--grm /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/mega_grm/20200330_mega_grm_relatedness_cutoff_0.05 \\
--qcovar ",qcovar_file," \\
--reml \\
--out ",results.dir,"/${phenoname} \\
--thread-num 10 ")

    slurm.path=file.path(out.dir,paste0("sex_specific_lab_h2.slurm"))
    write(slurm, file=slurm.path)

    cmd=paste0("sbatch ", slurm.path)
    system(cmd)
}

print("find jobs that timed out")
timed.out <- sex.n.complete[!(sex.n.complete$lab %in% sex.spec$lab),]
print(paste0("number of jobs that timed out:", nrow(timed.out)))

print(paste0(timed.out$lab))

if(nrow(timed.out)>0){
    print("re-run jobs that timed out")
    timed.out.job.numbers <- match(timed.out$lab, names(pheno))
    timed.out.job.numbers2 <- paste0(timed.out.job.numbers, collapse=",")

    results.dir <- path

    slurm=paste0("#!/bin/bash
#SBATCH -n1
#SBATCH --mem=150GB
#SBATCH --time=48:00:00
#SBATCH --cpus-per-task 3
#SBATCH --array=",timed.out.job.numbers2,"
#SBATCH --output=",paste0(out.dir,"timed.out_h2_reml_output.txt"),"
#SBATCH --mail-user=",email,"
#SBATCH --mail-type=ALL
#SBATCH --job-name=timed.out_lab_h2
#SBATCH --account=davis_lab

phenoname=$(awk 'FNR == 1 {print $3}' ",out.dir,"/pheno_temp/${SLURM_ARRAY_TASK_ID}_*.txt)

/data/davis_lab/sealockj/projects/scripts/lab_gwas/gcta_1.92.4beta/gcta64 \\
--pheno ",out.dir,"/pheno_temp/${SLURM_ARRAY_TASK_ID}_${phenoname}.txt \\
--grm /data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/mega_grm/20200330_mega_grm_relatedness_cutoff_0.05 \\
--qcovar ",qcovar_file," \\
--covar ",sex_cov_file," \\
--reml \\
--out ",results.dir,"/${phenoname} \\
--thread-num 10 ")

    slurm.path=file.path(out.dir,paste0("timed.out_lab_h2.slurm"))
    write(slurm, file=slurm.path)

    cmd=paste0("sbatch ", slurm.path)
    system(cmd)
}

